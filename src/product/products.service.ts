import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductsDto } from './dto/create-products.dto';
import { UpdateProductsDto } from './dto/update-products.dto';
import { Products } from './entities/products.entity';

let products: Products[] = [
  { id: 1, name: 'ToyGun-RPG', price: 50000 },
  { id: 2, name: 'ToyGun-UMP', price: 40000 },
  { id: 3, name: 'ToyGun-AKM', price: 30000 },
];
let lastUserId = 4;
@Injectable()
export class ProductsService {
  create(createProductsDto: CreateProductsDto) {
    const newProducts: Products = {
      id: lastUserId++,
      ...createProductsDto,
    };
    products.push(newProducts);
    return newProducts;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((pro) => {
      return pro.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductsDto: UpdateProductsDto) {
    const index = products.findIndex((pro) => {
      return pro.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProducts: Products = {
      ...products[index],
      ...updateProductsDto,
    };
    products[index] = updateProducts;
    return updateProducts;
  }

  remove(id: number) {
    const index = products.findIndex((pro) => {
      return pro.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProducts = products[index];
    products.splice(index, 1);
    return deleteProducts;
  }

  reset() {
    products = [
      { id: 1, name: 'ToyGun-RPG', price: 50000 },
      { id: 2, name: 'ToyGun-UMP', price: 40000 },
      { id: 3, name: 'ToyGun-AKM', price: 30000 },
    ];
    lastUserId = 4;
    return 'RESET';
  }
}
