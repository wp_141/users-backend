import { IsNotEmpty, IsPositive, MinLength } from 'class-validator';
export class CreateProductsDto {
  @MinLength(8)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  price: number;
}
